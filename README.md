# README #

This repository is to address the following requirement


Write a java application that takes a string of space separated numbers from 0 to 10 and calculates what that would be as a score in ten pin bowling (hints on how to score in bowling are here: http://www.bowling2u.com/trivia/game/scoring.asp). 

If a final score cannot be determined from the input then method should return the "current" score (i.e. assumes any remaining bowls scored 0).  

Example inputs and outputs

"1 2 3 4" -> 10
"9 1 9 1" -> 29
"1 1 1 1 10 1 1" -> 18
"10 10 10 10 10 10 10 10 10 10 10 10" -> 300

### How do set up? ###
Clone the branch and add into eclipse as Java project
