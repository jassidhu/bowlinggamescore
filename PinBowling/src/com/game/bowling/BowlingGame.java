package com.game.bowling;
public class BowlingGame {

	private int rolls[] = new int[21];
	private int currentRoll = 0;

	/**
	 * This method will validate input string
	 * @param inputString
	 * @return
	 */
	public boolean validateInputScore(String inputString){	
		//validate string for numbers and spaces
		return inputString.matches("[0-9 ]+");
	}
	/**
	 * Populate String array with score from inputScoreString.
	 * @param scoreString
	 */
	public void populateFrameScore(String scoreString) {
		String inputString[]= scoreString.split(" ");
		for(String str : inputString){
			rolls[currentRoll++] = Integer.parseInt(str);
		}		
	}

	/**
	 * This method will read Pin score Frame by Frame and determine final Score
	 * if scoreString is not in right format or contain invalid inputs then method will return 0
	 * @return int
	 */
	public int calculateScore(String scoreString) {
		int gameScore = 0;
		int frameIndex = 0;
		if(validateInputScore(scoreString)){
			populateFrameScore(scoreString);
			for (int frame = 0; frame < 10; frame++) {
				if (isStrike(frameIndex)) {
					//GameScore= gameScore + 10 + StrikeBonus
					int strikeBonusScore= rolls[frameIndex + 1] + rolls[frameIndex + 2];
					gameScore += 10 + strikeBonusScore;
					frameIndex++;
				} else if (isSpare(frameIndex)) {
					//GameScore= gameScore + 10 + SpareBonus
					gameScore += 10 + rolls[frameIndex + 2];
					frameIndex += 2;
				} else {
					gameScore += scoreInFrame(frameIndex);
					frameIndex += 2;
				}
			}
		}
		return gameScore;
	}

	private boolean isStrike(int frameIndex) {
		return rolls[frameIndex] == 10;
	}

	private int scoreInFrame(int frameIndex) {
		return rolls[frameIndex] + rolls[frameIndex + 1];
	}

	private boolean isSpare(int frameIndex) {
		return rolls[frameIndex] + rolls[frameIndex + 1] == 10;
	}

}
