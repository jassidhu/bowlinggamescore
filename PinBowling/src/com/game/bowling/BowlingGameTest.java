package com.game.bowling;
import static org.junit.Assert.*;

import org.junit.Test;
public class BowlingGameTest  {

	@Test
	public void test(){
		BowlingGame obj = new BowlingGame();
		String inputString= "1 2 3 4";
		assertEquals(obj.calculateScore(inputString), 10);
	}
	
	@Test
	public void testScenario2(){
		BowlingGame obj = new BowlingGame();
		String inputString= "9 1 9 1";
		assertEquals(obj.calculateScore(inputString), 29);
	}
	
	@Test
	public void testScenario3(){
		BowlingGame obj = new BowlingGame();
		String inputString= "1 1 1 1 10 1 1";
		assertEquals(obj.calculateScore(inputString), 18);
	}

	@Test
	public void testScenario4(){
		BowlingGame obj = new BowlingGame();
		String inputString= "10 10 10 10 10 10 10 10 10 10 10 10";
		assertEquals(obj.calculateScore(inputString), 300);
	}
}
